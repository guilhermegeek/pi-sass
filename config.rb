require 'susy'
require 'sassy-buttons'
require 'normalize-scss'
require 'breakpoint'
require 'animate-sass'
require 'font-awesome-sass'

project_type = :stand_alone
line_comments = false
preferred_syntax = :scss
output_style = :expanded
relative_assets = true
http_path = "src"
css_dir = "release"
sass_dir = "src"
javascripts_dir = "web/public/dist"